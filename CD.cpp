#include <string>
#include <iostream>
#include <vector>
#include <iomanip>
#include "CD.h"
#include "DVD.h"

/*
 CD.cpp
 Created: 11/1/2021
 Updated: 21/1/2021
 */
CD::CD(){};
CD::CD(std::string album, std::string title, int year, int Pid, float cost, int stock) : DVD(title, year, Pid, cost, stock)
{
  this->album = album;
}

std::string CD::getAlbum()
{
  return album;
}

std::vector<std::string> CD::Display()
{
  std::cout << "\n\n"
            << std::string(84, '-') << "\n";
  std::cout << "|";
  return {"PID", "Title", "Album", "Year", "Cost", "Stock"};
}
std::vector<std::string> CD::List()
{
  return {std::to_string(getPID()), getTitle(), getAlbum(),
          std::to_string(getYear()), std::to_string(getCost()),
          std::to_string(getStock())};
}