#ifndef _DVD_H_
#define _DVD_H_
#include "Product.h"
/*
 DVD.h
 Created: 13/1/2021
 Updated: 21/1/2021
*/

class DVD : public Product
{
private:
  std::string title;
  int year;

public:
  DVD();
  DVD(std::string title, int year, int Pid, float cost, int stock);

  int getYear();
  std::string getTitle();

  /*
    method to display DVD header while displaying
    @return display header in vector form
  */
  std::vector<std::string> Display();
  /*
    method for returning variables in vector form
    @return variable in vector
  */
  std::vector<std::string> List();
};
#endif