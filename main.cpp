#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <vector>
#include <fstream>
#include "Product.h"
#include "DVD.h"
#include "CD.h"
#include "Book.h"
#include "Magazine.h"
#include "CSVfile.h"
/*
 main.cpp
 Created: 11/1/2021
 Updated: 21/1/2021
 */

//Creating Global Class Objects
Product p;
Magazine m;
Book b;
CD c;
DVD d;

#define RED "\033[31m"  // Define RED colour for output
#define RESET "\033[0m" // Define RESET to return to orignal colour

void selectType(int);
void mainmenu();
void Getdata(std::string);
void WriteData(std::string);
void ReadData(std::string);
void SellStock(std::string);
void UpdateStock(std::string);
void ModifyStock(std::string);
std::string center(const std::string s, const int w);

/*
  Display the Menu option and then call selecType() to choose item type.
*/
void mainmenu()
{
  int choice;
  std::cout << "\e[8;35;115t";
  system("clear");

  std::cout << "\n\n\n\t\t\t|========= WELCOME TO STOCK MANAGEMENT =========|";
  std::cout << "\n\n\n\t\t\t\t (1) View Items " << std::endl;
  std::cout << "\t\t\t\t (2) Add Items " << std::endl;
  std::cout << "\t\t\t\t (3) Sell Items " << std::endl;
  std::cout << "\t\t\t\t (4) Update Stock " << std::endl;
  std::cout << "\t\t\t\t (5) Modify Items Stock" << std::endl;
  std::cout << "\t\t\t\t (6) Sales Report " << std::endl;
  std::cout << "\t\t\t\t (7) Exit " << std::endl;
  std::cout << "\n\n\t\t\t\t Enter Choice: ";
  std::cin >> choice;

  switch (choice)
  {
  case 1:
    selectType(choice);
    break;
  case 2:
    selectType(choice);
    break;
  case 3:
    selectType(choice);
    break;
  case 4:
    selectType(choice);
    break;
  case 5:
    selectType(choice);
    break;
  case 6:
    ReadData("Sales");
    mainmenu();
    break;
  case 7:
    std::cout << "\n\n\t\t\t ThankYou For Using Stock Management." << std::endl;
    exit(0);
    break;
  default:
    std::cout << RED << "\n\n\n\t\t\t\t Wrong Choice Entered! Try Again" << RESET;
    mainmenu();
    break;
  }
}

/*
  Display the Item options and then perform related action which is taken from mainmenu().
  @param function , used to tell whether to use the item to read, sell etc.
*/
void selectType(int function)
{
  system("clear");
  int choice;
  std::cout << "\n\n\n\t\t\t|========= Please Select Type =========|";
  std::cout << "\n\n\n\t\t\t\t (1) CD " << std::endl;
  std::cout << "\t\t\t\t (2) DVD " << std::endl;
  std::cout << "\t\t\t\t (3) Magazine " << std::endl;
  std::cout << "\t\t\t\t (4) Book " << std::endl;
  std::cout << (function == 1 ? "\t\t\t\t (5) View All " : "");
  std::cout << "\n\t\t\t\t (0) Go Back " << std::endl;
  std::cout << "\n\n\t\t\t\t Enter Choice: ";
  std::cin >> choice;
  switch (choice) //switch case which based on item type performs other functions selected from the main menu
  {
  case 1:
    if (function == 1)
      ReadData("CD");
    else if (function == 2)
      Getdata("CD");
    else if (function == 3)
      SellStock("CD");
    else if (function == 4)
      UpdateStock("CD");
    else if (function == 5)
      ModifyStock("CD");

    break;
  case 2:
    if (function == 1)
      ReadData("DVD");
    else if (function == 2)
      Getdata("DVD");
    else if (function == 3)
      SellStock("DVD");
    else if (function == 4)
      UpdateStock("DVD");
    else if (function == 4)
      ModifyStock("DVD");
    ;
    break;
  case 3:
    if (function == 1)
      ReadData("Magazine");
    else if (function == 2)
      Getdata("Magazine");
    else if (function == 3)
      SellStock("Magazine");
    else if (function == 4)
      UpdateStock("Magazine");
    else if (function == 5)
      ModifyStock("Magazine");
    break;
  case 4:
    if (function == 1)
      ReadData("Book");
    else if (function == 2)
      Getdata("Book");
    else if (function == 3)
      SellStock("Book");
    else if (function == 4)
      UpdateStock("Book");
    else if (function == 5)
      ModifyStock("Book");
    break;
  case 5:
    ReadData("CD");
    ReadData("DVD");
    ReadData("Magazine");
    ReadData("Book");
    break;
  case 0:
    mainmenu();
    break;
  default:
    std::cout << RED << "\n\n\n\t\t\t\t Wrong Choice Entered! Try Again" << RESET;
    system("clear");
    selectType(function);
    break;
  }
  mainmenu();
}

/*
  Asks user item related details and write data to CSV file using WriteData().
  @param type , type of item selected.
*/
void Getdata(std::string type)
{
  //variables to save item details
  std::string album, title, author;
  int year, Pid, stock, ISBN, issue;
  float cost;

  system("clear");
  std::cout << "\n\n\n\t\t\t\t Enter Product id: ";
  std::cin >> Pid;
  if (type == "CD" || type == "DVD")
  {
    std::cout << "\t\t\t\t Enter Title: ";
    std::cin >> title;
    if (type == "CD")
    {
      std::cout << "\t\t\t\t Enter Album: ";
      std::cin >> album;
    }
    std::cout << "\t\t\t\t Enter Year: ";
    std::cin >> year;
  }
  else if (type == "Magazine" || type == "Book")
  {
    std::cout << "\t\t\t\t Enter Title: ";
    std::cin >> title;
    std::cout << "\t\t\t\t Enter Author: ";
    std::cin >> author;
    if (type == "Magazine")
    {
      std::cout << "\t\t\t\t Enter Issue: ";
      std::cin >> issue;
    }
    std::cout << "\t\t\t\t Enter ISBN: ";
    std::cin >> ISBN;
    std::cout << "\t\t\t\t Enter Year: ";
    std::cin >> year;
  }
  std::cout << "\t\t\t\t Enter Cost: ";
  std::cin >> cost;
  std::cout << "\t\t\t\t Enter Current Stock: ";
  std::cin >> stock;

  //based on type writes details to class using constructor declaration.
  if (type == "CD")
    c = CD(title, album, year, Pid, cost, stock);
  else if (type == "DVD")
    d = DVD(title, year, Pid, cost, stock);
  else if (type == "Magazine")
    m = Magazine(title, author, ISBN, issue, year, Pid, cost, stock);
  else if (type == "Book")
    b = Book(title, author, ISBN, year, Pid, cost, stock);
  //function to write items details.
  WriteData(type);
  std::cout << "\n\t\t\t\t Data Saved to file." << std::endl;
  std::cout << "\n\n\t\t\t\t Enter to go back to Main Menu:";
  std::cin.get();
  std::cin.ignore(10, '\n');
}

/*
  Gets item details written to class in vector<string> form and saves it CSV file.
  @param type , type of item selected.
*/
void WriteData(std::string type)
{
  //variable to store returned vector from class.
  std::vector<std::string> dataList;
  dataList.clear();
  //create object of class CSVfile with item type as file name.
  CSVfile write(type + ".csv");

  if (type == "CD")
    dataList = {c.List()};
  else if (type == "DVD")
    dataList = {d.List()};
  else if (type == "Magazine")
    dataList = {m.List()};
  else if (type == "Book")
    dataList = {b.List()};
  else if (type == "Sales")
    dataList = {b.List()};

  //calls method addData() from CSVfile which writes the vector to the CSV file.
  write.addData(dataList.begin(), dataList.end());
}

/*
  Reads data from CSV file and display it to the user.
  @param type , type of item selected.
*/
void ReadData(std::string type)
{
  system("clear");
  //variable to store item details
  std::vector<std::vector<std::string>> dataList;
  dataList.clear();
  //variable used for table formation during display
  int f = 0;
  std::string file = type + ".csv";
  CSVfile read(file); // Get the data from CSV File
  std::cout << "\n\t\t Displaying Records Of " << type << ": ";
  dataList = read.getData(); // assigns the data recieved from CSV file
  if (dataList.empty())      // checks if the data returned is empty ( no data in csv file )
  {
    std::cin.get();
    return;
  }
  //displays data in a tabular manner.
  if (type == "CD")
  {
    f = 84;
    for (std::string data : c.Display())
      std::cout << center(data, 12) << "| ";
    std::cout << "\n"
              << std::string(f, '-') << "\n";
    for (std::vector<std::string> vec : dataList)
    {
      std::cout << "|";
      for (std::string data : vec)
        std::cout << center(data, 12) << "| ";
      std::cout << std::endl;
    }
    std::cout << std::string(f, '-') << "\n";
  }
  if (type == "DVD")
  {
    f = 70;
    for (std::string data : d.Display())
      std::cout << center(data, 12) << "| ";
    std::cout << "\n"
              << std::string(f, '-') << "\n";
    for (std::vector<std::string> vec : dataList)
    {
      std::cout << "|";
      for (std::string data : vec)
        std::cout << center(data, 12) << "| ";
      std::cout << std::endl;
    }
    std::cout << std::string(f, '-') << "\n";
  }
  if (type == "Magazine")
  {
    f = 112;
    for (std::string data : m.Display())
      std::cout << center(data, 12) << "| ";
    std::cout << "\n"
              << std::string(f, '-') << "\n";
    for (std::vector<std::string> vec : dataList)
    {
      std::cout << "|";
      for (std::string data : vec)
        std::cout << center(data, 12) << "| ";
      std::cout << std::endl;
    }
    std::cout << std::string(f, '-') << "\n";
  }
  if (type == "Book")
  {
    f = 98;
    for (std::string data : b.Display())
      std::cout << center(data, 12) << "| ";
    std::cout << "\n"
              << std::string(f, '-') << "\n";
    for (std::vector<std::string> vec : dataList)
    {
      std::cout << "|";
      for (std::string data : vec)
        std::cout << center(data, 12) << "| ";
      std::cout << std::endl;
    }
    std::cout << std::string(f, '-') << "\n";
  }
  if (type == "Sales")
  {
    f = 70;
    for (std::string data : p.Display())
      std::cout << center(data, 12) << "| ";
    std::cout << "\n"
              << std::string(f, '-') << "\n";
    for (std::vector<std::string> vec : dataList)
    {
      std::cout << "|";
      for (std::string data : vec)
        std::cout << center(data, 12) << "| ";
      std::cout << std::endl;
    }
    std::cout << std::string(f, '-') << "\n";
  }
  std::cout << "\n\n\t\t Press Enter Key To Continue.";
  std::cin.get();
  std::cin.ignore(10, '\n');
}

/*
  Sells items to the user and maintains a sales record.
  @param type , type of item selected.
*/
void SellStock(std::string type)
{
  /*
    pid - the user entered pid 
    PID - the pid returned from csv file
    count - counter to check if pid is found or not
  */
  int pid, PID, count = 0, i;
  ReadData(type); // display the item details for the user to tehn select
  std::string file1 = type + ".csv";
  std::string file2 = type + "2.csv";
  std::ifstream Read(file1); // ifstream to read from the file

  if (!Read) // checks if the data exists in the file
    mainmenu();
  else
  {
    /*
      index - index of stock in file
      buy - amounnt the user whishes to buy
      total - the total amount the user would pay
      currentstock - current stock
    */
    int index, buy, total, currentstock;
    std::string line, word;
    std::vector<std::string> row, report;
    std::ofstream Write(file2); // ofstream to write to the file
    // Determine the index of the Stock
    if (type == "CD")
      index = 5;
    else if (type == "DVD")
      index = 4;
    else if (type == "Magazine")
      index = 6;
    else if (type == "Book")
      index = 5;
    while (count < 1)
    {
      // Get the pid number from the user
      std::cout << "\n\n\t\t Enter a PID of Item to Sell: ";
      std::cin >> pid;

      // Traverse the file
      while (!Read.eof())
      {
        row.clear();
        getline(Read, line); //stores the line read into variable 'line'
        if (line == "")      // checks if its empty line
          continue;
        //breaks the line into words
        std::stringstream s(line);
        while (getline(s, word, ',')) // loop to remove the ',' and save to vector row
          row.push_back(word);
        PID = std::stoi(row[0]); // assigns the first element to PID
        int row_size = row.size();

        if (pid == PID) // compares the pid entered by the use and the one found
        {
          count = 1;
          currentstock = std::stoi(row[index]); // assigns the current stock found in data
          std::stringstream convert;            // intialize stringstream
          // do-while loop to enter the amount the user wants
          do
          {
            std::cout << "\n\t\t Enter the Amount to Buy: ";
            std::cin >> buy;
          } while (buy > currentstock); // loop keeps running if asked amount is greater then available

          convert << currentstock - buy;           // store the balance stock
          row[index] = convert.str();              // sending a number as a stream into output string
          total = buy * std::stoi(row[index - 1]); //calculates total ( amount bought * cost )

          // inserts type, title, buy, cost, total to vector report ( all in string format )
          report.insert(report.end(), {type, row[1], std::to_string(buy), row[index - 1], std::to_string(total)});

          CSVfile write("Sales.csv"); // create a object of CSVfile to write
          std::cout << "\n\n\t\t Sale of " << buy << " items of type " << type << " confirmed. ";

          write.addData(report.begin(), report.end()); // writes teh report vector to Sales.csv

          for (i = 0; i < row_size - 1; i++) // loop to write changed data
          {
            Write << row[i] << ",";
          }
          Write << row[row_size - 1];
        }
        else if (!Read.eof())
        {
          for (i = 0; i < row_size - 1; i++) // loop to write unchanged data
          {
            Write << row[i] << ",";
          }
          // the last column data ends with a '\n'
          Write << row[row_size - 1] << "\n";
        }
        else
          break;
      }
      Read.clear();
      Read.seekg(0, std::ios::beg); // moves the pointer to begining of record
    }
    //close fstream
    Read.close();
    Write.close();
    remove(file1.c_str());                // removing the existing file
    rename(file2.c_str(), file1.c_str()); // renaming the updated file with the existing file name
    std::cout << "\n\n\t\t Press Enter Key To Continue.";
    std::cin.get();
    std::cin.ignore(10, '\n');
  }
}

/*
  Display the string centralised to the width sent.
  @param string , string to be centralised.
  @param width , lenght of which string need to be center
  @return teh string in center of and rest filled with spaces
*/
std::string center(const std::string string, const int width)
{
  std::stringstream ss, spaces;
  int padding = width - string.size(); // count excess room to pad
  for (int i = 0; i < padding / 2; ++i)
    spaces << " ";
  ss << spaces.str() << string << spaces.str(); // format with padding
  if (padding > 0 && padding % 2 != 0)          // if odd #, add 1 space
    ss << " ";
  return ss.str();
}

/*
  Updates data of particular item.
  @param file , type of item selected.
*/
void UpdateStock(std::string file)
{
  /*
    pid - the user entered pid 
    PID - the pid returned from csv file
    count - counter to check if pid is found or not
  */
  int pid, PID, count = 0, i;
  ReadData(file); // call function ReadData() to display item details
  std::string file1 = file + ".csv";
  std::string file2 = file + "2.csv";
  std::ifstream Read(file1);  // ifstream to read data from
  std::ofstream Write(file2); // ofstream to write the updated details

  if (!Read) // checks if the item file is empty
    mainmenu();
  else
  {
    /* 
      index - index of stock in file
      nstock - new stock entered by the user
    */
    int index, nstock;
    std::string line, word, sub;
    std::vector<std::string> row; // stores the updated details
    // Determine the index of the Stock
    if (file == "CD")
      index = 5;
    else if (file == "DVD")
      index = 4;
    else if (file == "Magazine")
      index = 6;
    else if (file == "Book")
      index = 5;

    while (count < 1) // loop to ask user again if pid not found
    {
      // Get the pid number from the user
      std::cout << "\n\n\t\t Enter a PID number of the record to be updated: ";
      std::cin >> pid;

      // Traverse the file
      while (!Read.eof())
      {
        row.clear();
        getline(Read, line); // save the line read to variable 'line'
        std::stringstream s(line);

        while (getline(s, word, ',')) // loop to remove ',' from line and get only words
          row.push_back(word);        // push only words to vector row

        PID = std::stoi(row[0]); // assign the PID from vector index
        int row_size = row.size();

        if (pid == PID && line != "") // check if pid id found and line is empty
        {
          count = 1;
          std::stringstream convert;
          // Get the new stock
          std::cout << "\n\t\t Enter new Stock: ";
          std::cin >> nstock;
          // sending a number as a stream into output string
          convert << nstock;
          // the str() converts number into string
          row[index] = convert.str();
          // sending a number as a stream into output string

          for (i = 0; i < row_size - 1; i++) // write updated data to new file
          {
            Write << row[i] << ",";
          }
          Write << row[row_size - 1] << "\n";
        }
        else if (!Read.eof())
        {
          for (i = 0; i < row_size - 1; i++) // write the rest unchanged data to new file
          {
            Write << row[i] << ",";
          }
          // the last column data ends with a '\n'
          Write << row[row_size - 1] << "\n";
        }
        else
          break;
      }
      Read.clear();
      Read.seekg(0, std::ios::beg); // position pointer to start
    }
    // close ifstream and ofstraem
    Read.close();
    Write.close();

    // removing the existing file
    remove(file1.c_str());
    // renaming the updated file with the existing file name
    rename(file2.c_str(), file1.c_str());
    std::cout << "\n\n\t\t Press Enter Key To Continue.";
    std::cin.get();
    std::cin.ignore(10, '\n');
  }
}

/*
  Modifies data of particular item.
  @param file , type of item selected.
*/
void ModifyStock(std::string type)
{
  // variables store new item details
  std::string album, title, author;
  int year, stock, ISBN, issue, count = 0;
  float cost;
  std::string pid;                                // to search fro pid in the file
  std::vector<std::vector<std::string>> datalist; // vector to store item details
  datalist.clear();

  std::string file = type + ".csv";
  CSVfile read(file);            // Get the data from CSV File
  CSVfile write(type + "2.csv"); // Write data to new CSV file
  datalist = read.getData();     // read data from file and store it in vector form
  ReadData(type);                // display teh existing details
  std::cout << "\n\t\t Enter Pid of Data to Modify: ";
  std::cin >> pid;
  for (auto &stringVector : datalist) // loop to search through the vector for pid
  {
    auto it = std::find(stringVector.begin(), stringVector.end(), pid);
    // checks if pid is found and at specific postion ( index 0 )
    if (it != stringVector.end() && std::distance(stringVector.begin(), it) == 0)
    {
      count++; // updates counter that pid is found
      // Accepts the new details from user
      if (type == "CD" || type == "DVD")
      {
        std::cout << "\t\t\t Enter Title: ";
        std::cin >> title;
        if (type == "CD")
        {
          std::cout << "\t\t\t Enter Album: ";
          std::cin >> album;
        }
        std::cout << "\t\t\t Enter Year: ";
        std::cin >> year;
      }
      else if (type == "Magazine" || type == "Book")
      {
        std::cout << "\t\t\t Enter Title: ";
        std::cin >> title;
        std::cout << "\t\t\t Enter Author: ";
        std::cin >> author;
        if (type == "Magazine")
        {
          std::cout << "\t\t\t Enter Issue: ";
          std::cin >> issue;
        }
        std::cout << "\t\t\t Enter ISBN: ";
        std::cin >> ISBN;
        std::cout << "\t\t\t Enter Year: ";
        std::cin >> year;
      }
      std::cout << "\t\t\t Enter Cost: ";
      std::cin >> cost;
      std::cout << "\t\t\t Enter Current Stock: ";
      std::cin >> stock;
      // depending on type stores the new details to stringVector
      if (type == "CD")
        stringVector = {pid, title, album, std::to_string(year), std::to_string(cost), std::to_string(stock)};
      else if (type == "DVD")
        stringVector = {pid, title, std::to_string(year), std::to_string(cost), std::to_string(stock)};
      else if (type == "Magazine")
        stringVector = {pid, title, author, std::to_string(issue), std::to_string(year), std::to_string(ISBN), std::to_string(cost), std::to_string(stock)};
      else if (type == "Book")
        stringVector = {pid, title, author, std::to_string(year), std::to_string(ISBN), std::to_string(cost), std::to_string(stock)};
      break;
    }
  }
  if (count != 1) // if pid not found ask starts again
  {
    std::cout << "\n\t\t Pid Not Found ";
    std::cout << "\n\t\t Press Enter Key To Try Again.";
    std::cin.get();
    std::cin.ignore(10, '\n');
    ModifyStock(type);
  }
  else
  { // if pid is found writes the data to a new file and removes teh old file
    std::cout << "\n\n\t\t Data Modified!";
    for (long unsigned int i = 0; i < datalist.size(); i++)
    {
      write.addData(datalist[i].begin(), datalist[i].end());
      std::cout << std::endl;
    }
    remove(file.c_str()); // removing the existing file
    // renaming the updated file with the existing file name
    rename((type + "2.csv").c_str(), file.c_str());
  }
  std::cout << "\t\t Press Enter Key To Continue.";
  std::cin.get();
  std::cin.ignore(10, '\n');
}

int main()
{
  // calls function mainmenu()
  mainmenu();

  return 0;
}