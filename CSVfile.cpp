#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include "CSVfile.h"
/*
 CSVfile.cpp
 Created: 14/1/2021
 Updated: 17/1/2021
 */
#define RED "\033[31m" /* Red */
#define RESET "\033[0m"

CSVfile::CSVfile(std::string filename, std::string delm) : fileName(filename), delimeter(delm), linesCount(0)
{
}
CSVfile::CSVfile(){};

//
std::vector<std::vector<std::string>> CSVfile::getData()
{
  std::ifstream file(fileName);
  std::vector<std::vector<std::string>> dataList;
  std::string line, word;
  if (!file)
  {
    std::cout << RED << "\n\n\t\t File Empty " << RESET << "\n\t\t Add Data ";
    std::cout << "\n\n\t\t Press Enter Key To Continue.";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  else
  {
    while (getline(file, line))
    {
      std::vector<std::string> vec;
      std::stringstream ss(line);

      // Iterate through each line and split the content using delimeter
      while (getline(ss, word, ','))
        vec.push_back(word);
      dataList.push_back(vec); // add wprd to vector dataList
    }
    file.close();
  }
  return dataList;
}