#include <string>
#include <vector>
#include <iostream>
#include "Magazine.h"
#include "Book.h"

/*
 Magazine.cpp
 Created: 13/1/2021
 Updated: 21/1/2021
 */
Magazine::Magazine(){};
Magazine::Magazine(std::string title, std::string author, int ISBN, int issue, int year, int Pid, float cost, int stock)
    : Book(title, author, ISBN, year, Pid, cost, stock)
{
  this->issue = issue;
}

int Magazine::getIssue()
{
  return issue;
}

std::vector<std::string> Magazine::Display()
{
  std::cout << "\n\n"
            << std::string(112, '-') << "\n";
  std::cout << "|";
  return {"PID", "Title", "Author", "Issue", "Year", "ISBN", "Cost", "Stock"};
}

std::vector<std::string> Magazine::List()
{
  return {std::to_string(getPID()), getTitle(), getAuthor(), std::to_string(getIssue()),
          std::to_string(getYear()), std::to_string(getISBN()), std::to_string(getCost()), std::to_string(getStock())};
}
