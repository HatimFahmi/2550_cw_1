#include <string>
#include <iostream>
#include <vector>
#include "Book.h"
#include "Product.h"

/*
 Book.cpp
 Created: 13/1/2021
 Updated: 21/1/2021
 */
Book::Book(){};
Book::Book(std::string title, std::string author, int ISBN, int year, int Pid, float cost, int stock) : Product(Pid, cost, stock)
{
  this->title = title;
  this->author = author;
  this->year = year;
  this->ISBN = ISBN;
}

int Book::getYear()
{
  return year;
}
int Book::getISBN()
{
  return ISBN;
}
std::string Book::getTitle()
{
  return title;
}
std::string Book::getAuthor()
{
  return author;
}

std::vector<std::string> Book::Display()
{
  std::cout << "\n\n"
            << std::string(98, '-') << "\n";
  std::cout << "|";
  return {"PID", "Title", "Author", "Year", "ISBN", "Cost", "Stock"};
}
std::vector<std::string> Book::List()
{
  return {std::to_string(getPID()), getTitle(), getAuthor(), std::to_string(getYear()),
          std::to_string(getISBN()), std::to_string(getCost()),
          std::to_string(getStock())};
}
