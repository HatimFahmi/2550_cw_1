#ifndef _CD_H_
#define _CD_H_
#include "DVD.h"

/*
 CD.h
 Created: 13/1/2021
 Updated: 21/1/2021
*/

class CD : public DVD
{
private:
  std::string album;

public:
  CD();
  CD(std::string album, std::string title, int year, int Pid, float cost, int stock);

  std::string getAlbum();

  /*
    method to display CD header while displaying
    @return display header in vector form
  */
  std::vector<std::string> Display();
  /*
    method for returning variables in vector form
    @return variable in vector
  */
  std::vector<std::string> List();
};
#endif