#ifndef _PRODUCT_H_
#define _PRODUCT_H_
/*
 Product.h
 Created: 11/1/2021
 Updated: 13/1/2021
*/

class Product
{
private:
  int Pid;
  float cost;
  int stock;

public:
  Product();
  Product(int Pid, float cost, int stock); // constructor to intialize values

  int getPID();
  float getCost();
  int getStock();

  //method to display sales report header when displaying
  std::vector<std::string> Display();
};
#endif