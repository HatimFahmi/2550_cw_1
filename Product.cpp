#include <string>
#include <vector>
#include <iostream>
#include "Product.h"
/*
 Product.cpp
 Created: 11/1/2021
 Updated: 13/1/2021
 */
Product::Product(){};
Product::Product(int Pid, float cost, int stock)
{
  this->Pid = Pid;
  this->cost = cost;
  this->stock = stock;
}

int Product::getPID()
{
  return this->Pid;
}

float Product::getCost()
{
  //float roundc = round(this->cost);
  return this->cost;
}

int Product::getStock()
{
  return this->stock;
}

std::vector<std::string> Product::Display()
{
  std::cout << "\n\n"
            << std::string(70, '-') << "\n";
  std::cout << "|";
  return {"Type", "Title", "Stock", "Cost", "Total"};
}
