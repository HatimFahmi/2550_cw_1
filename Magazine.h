#ifndef _MAGAZINE_H_
#define _MAGAZINE_H_
#include <string>
#include "Book.h"

/*
 Magazine.h
 Created: 13/1/2021
 Updated: 20/1/2021
*/

class Magazine : public Book
{
private:
  int issue;

public:
  Magazine();
  Magazine(std::string title, std::string author, int ISBN, int issue, int year, int Pid, float cost, int stock);

  int getIssue();

  /*
    method to display Magazine header while displaying
    @return display header in vector form
  */
  std::vector<std::string> Display();
  /*
    method for returning variables in vector form
    @return variable in vector
  */
  std::vector<std::string> List();
};
#endif