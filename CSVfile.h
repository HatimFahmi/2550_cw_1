#ifndef _CSVfile_H_
#define _CSVfile_H_
#include <fstream>
/*
 CSVfile.h
 Created: 14/1/2021
 Updated: 21/1/2021
*/
class CSVfile
{
private:
  std::string fileName;
  std::string delimeter;
  int linesCount;

public:
  CSVfile();
  CSVfile(std::string filename, std::string delm = ",");

  template <typename T>
  /*
    Writes data recived in vector form to CSV file
    @param first - start of vector
    @param last - end of vector
  */
  void addData(T first, T last)
  {
    std::fstream file;
    // Open the file in Append Mode if it exists
    file.open(fileName, std::ios::out | std::ios::app);

    // Iterate over the range and add each element to file seperated by delimeter.
    for (; first != last;)
    {
      file << *first;
      if (++first != last)
        file << delimeter;
    }
    linesCount++;
    file << std::endl;
    // Close the file
    file.close();
  }
  /*
    Read data from csv
    @return the data read from CSV file
  */
  std::vector<std::vector<std::string>> getData();
};
#endif