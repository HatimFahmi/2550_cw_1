#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "catch.hpp"
#include "../CSVfile.h"
#include "../Product.h"
#include "../CD.h"
#include "../DVD.h"
#include "../Book.h"
#include "../Magazine.h"

/*
 test_case.cpp
 Created: 22/1/2021
 Updated: 22/1/2021
 */

Product p;
Magazine m;
Book b;
CD c;
DVD d;

std::string center(const std::string string, const int width)
{
    std::stringstream ss, spaces;
    int padding = width - string.size(); // count excess room to pad
    for (int i = 0; i < padding / 2; ++i)
        spaces << " ";
    ss << spaces.str() << string << spaces.str(); // format with padding
    if (padding > 0 && padding % 2 != 0)          // if odd #, add 1 space
        ss << " ";
    return ss.str();
}

void Getdata(std::string type)
{
    //variables to save item details
    std::string album, title;
    int year, Pid, stock;
    float cost;

    system("clear");
    std::cout << "\n\n\n\t\t\t\t Enter Product id: ";
    std::cin >> Pid;
    if (type == "CD" || type == "DVD")
    {
        std::cout << "\t\t\t\t Enter Title: ";
        std::cin >> title;
        if (type == "CD")
        {
            std::cout << "\t\t\t\t Enter Album: ";
            std::cin >> album;
        }
        std::cout << "\t\t\t\t Enter Year: ";
        std::cin >> year;
    }
    std::cout << "\t\t\t\t Enter Cost: ";
    std::cin >> cost;
    std::cout << "\t\t\t\t Enter Current Stock: ";
    std::cin >> stock;

    //based on type writes details to class using constructor declaration.
    if (type == "CD")
        c = CD(title, album, year, Pid, cost, stock);
    //function to write items details.
    std::cout << "\n\t\t\t\t Data Saved to file." << std::endl;
}

void ReadData(std::string type)
{
    system("clear");
    //variable to store item details
    std::vector<std::vector<std::string>> dataList;
    dataList.clear();
    //variable used for table formation during display
    int f = 0;
    std::string file = type + ".csv";
    CSVfile read(file); // Get the data from CSV File
    std::cout << "\n\t\t Displaying Records Of " << type << ": ";
    dataList = read.getData(); // assigns the data recieved from CSV file
    if (dataList.empty())      // checks if the data returned is empty ( no data in csv file )
    {
        std::cin.get();
        return;
    }
    //displays data in a tabular manner.
    if (type == "CD")
    {
        f = 84;
        for (std::string data : c.Display())
            std::cout << center(data, 12) << "| ";
        std::cout << "\n"
                  << std::string(f, '-') << "\n";
        for (std::vector<std::string> vec : dataList)
        {
            std::cout << "|";
            for (std::string data : vec)
                std::cout << center(data, 12) << "| ";
            std::cout << std::endl;
        }
        std::cout << std::string(f, '-') << "\n";
    }
    std::cout << "\n\n\t\t Press Enter Key To Continue.";
    std::cin.get();
    std::cin.ignore(10, '\n');
}

void mainmenu()
{
    int choice;
    std::cout << "\e[8;35;115t";
    system("clear");

    std::cout << "\n\n\n\t\t\t|========= WELCOME TO STOCK MANAGEMENT =========|";
    std::cout << "\n\n\n\t\t\t\t (1) View Items " << std::endl;
    std::cout << "\t\t\t\t (2) Add Items " << std::endl;
    std::cout << "\t\t\t\t (3) Sell Items " << std::endl;
    std::cout << "\t\t\t\t (4) Update Stock " << std::endl;
    std::cout << "\t\t\t\t (5) Modify Items Stock" << std::endl;
    std::cout << "\t\t\t\t (6) Sales Report " << std::endl;
    std::cout << "\t\t\t\t (7) Exit " << std::endl;
    std::cout << "\n\n\t\t\t\t Enter Choice: ";
    std::cin >> choice;

    switch (choice)
    {
    case 1:
        // selectType(choice);
        break;
    case 2:
        // selectType(choice);
        break;
    case 3:
        //selectType(choice);
        break;
    case 4:
        //selectType(choice);
        break;
    case 5:
        //selectType(choice);
        break;
    case 6:
        ReadData("Sales");
        mainmenu();
        break;
    case 7:
        std::cout << "\n\n\t\t\t ThankYou For Using Stock Management." << std::endl;
        exit(0);
        break;
    default:
        mainmenu();
        break;
    }
}

TEST_CASE("Test for getting data from CSV file", "[CSVfile::getData()]")
{
    std::vector<std::string> input = {"aaa", "bbb", "ccc"};
    std::vector<std::vector<std::string>> expected_output =
        {{"aaa", "bbb", "ccc"}, {"ddd", "eee", "fff"}};
    CSVfile c("test.csv");
    c.addData(input.begin(), input.end());
    input = {"ddd", "eee", "fff"};
    c.addData(input.begin(), input.end());
    REQUIRE(c.getData() == expected_output);
    remove("test.csv");
}

TEST_CASE("Test for putting data into CSV file", "[CSVfile::addData()]")
{
    std::vector<std::string> input = {"aaa", "bbb", "ccc"};
    CSVfile c("test.csv");
    REQUIRE_NOTHROW(c.addData(input.begin(), input.end()));
    remove("test.csv");
}

TEST_CASE("Test for displaying sales header", "[Product::Display()]")
{
    std::vector<std::string> expected_output =
        {"Type", "Title", "Stock", "Cost", "Total"};
    Product p;
    std::vector<std::string> output = p.Display();
    REQUIRE(output == expected_output);
}

TEST_CASE("Test for getting data from user based on item type", "[Getdata()]")
{
    REQUIRE_NOTHROW(Getdata("CD"));
}

TEST_CASE("Test for reading data and then displaying to user", "[ReadData()]")
{
    //(title, album, year, Pid, cost, stock)
    remove("CD.csv");
    std::vector<std::string> input = {"aaa", "bbb", "ccc", "ddd", "eee", "fff"};

    CSVfile c("CD.csv");
    c.addData(input.begin(), input.end());
    REQUIRE_NOTHROW(ReadData("CD"));
    remove("CD.csv");
}

TEST_CASE("Test for making string center for display", "[center()]")
{
    std::string expected_output = "    PID     ";
    REQUIRE(center("PID", 12) == expected_output);
}

TEST_CASE("Test for getting data from class in vector form", "[DVD::List()]")
{
    std::vector<std::string> expected_output = {"1", "aaa", "2018", "5.000000", "20"};

    DVD d("aaa", 2018, 1, 5, 20);

    REQUIRE(d.List() == expected_output);
}

TEST_CASE("Test working of Main Menu", "[mainmenu()]")
{
    REQUIRE_NOTHROW(mainmenu());
}