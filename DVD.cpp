#include <string>
#include <vector>
#include <iostream>
#include "DVD.h"
#include "Product.h"

/*
 DVD.cpp
 Created: 11/1/2021
 Updated: 21/1/2021
 */
DVD::DVD(){};
DVD::DVD(std::string title, int year, int Pid, float cost, int stock) : Product(Pid, cost, stock)
{
  this->title = title;
  this->year = year;
}

int DVD::getYear()
{
  return year;
}
std::string DVD::getTitle()
{
  return title;
}

std::vector<std::string> DVD::Display()
{
  std::cout << "\n\n"
            << std::string(70, '-') << "\n";
  std::cout << "|";
  return {"PID", "Title", "Year", "Cost", "Stock"};
}
std::vector<std::string> DVD::List()
{
  return {
      std::to_string(getPID()), getTitle(), std::to_string(getYear()), std::to_string(getCost()),
      std::to_string(getStock())};
}