#ifndef _BOOK_H_
#define _BOOK_H_
#include "Product.h"
/*
 Book.h
 Created: 13/1/2021
 Updated: 21/1/2021
*/

class Book : public Product
{
private:
  std::string author;
  int ISBN;
  int year;
  std::string title;

public:
  Book();
  Book(std::string title, std::string author, int ISBN, int year, int Pid, float cost, int stock);

  int getYear();
  int getISBN();
  std::string getAuthor();
  std::string getTitle();

  /*
    method to display Book header while displaying
    @return display header in vector form
  */
  std::vector<std::string> Display();
  /*
    method for returning variables in vector form
    @return variable in vector
  */
  std::vector<std::string> List();
};
#endif